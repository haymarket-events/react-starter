import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import { Layout } from './components/Layout';
import './App.css';

// import ExchangeRateView from "./view";
import { Home } from './components/Home/Home';
import { Speaker } from './components/Speaker/Speaker';

export default class App extends Component {
  render() {
    return (
      <section className="App">
      <Layout>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/jury' component={Speaker} />          
        </Switch>
      </Layout>
      </section>
    );
  }
}
