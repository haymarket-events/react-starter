// graphQL
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from "apollo-link-http";
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-boost';
import gql from "graphql-tag";
// React router
import { BrowserRouter, HashRouter } from 'react-router-dom'
//
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const client = new ApolloClient({
    // uri: process.env.REACT_APP_GRAPHQL_URI,
    link: new HttpLink({
      // uri: `https://w5xlvm3vzz.lp.gql.zone/graphql`
      // uri: `http://localhost:3001/graphql`,
      uri: process.env.REACT_APP_GRAPHQL_URI
    }),
    // cache: new InMemoryCache()
    // for SSR, use:
    cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
    ssrForceFetchDelay: 1000,
});

// ReactDOM.render(<App />, document.getElementById('root'));
const rootElement = document.getElementById('root');

ReactDOM.render(
  <HashRouter>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </HashRouter>,
  rootElement
)
registerServiceWorker();
