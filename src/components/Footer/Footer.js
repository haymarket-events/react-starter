import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Query } from "react-apollo";

import { ActivityIndicator } from "react-native-web";

import { FooterView } from './Footer.view';
import { Query as FooterQuery } from './Footer.query';
import { fontSize, colors } from "./../../styles";

export class Footer extends Component {
  constructor() {
    super();
    this.state = {
      eventName : `digitalmediaawards-china-2018`,
      query : FooterQuery 
    }
  }
  render() {
    return (
      <section className="nav-menu">
        {/* <div>
          [<Link to="/">Home</Link>]
          [<Link to="/speakers">Speakers</Link>]
        </div>
        <hr /> */}
        <Query query={ this.state.query } variables={{ name: this.state.eventName }}>
          {({ loading, error, data }) => {
              if(error) {
                return (<pre>{JSON.stringify(error, null, 2)}</pre>)
              }
              if (loading || !data) { return <ActivityIndicator color={colors.teal} /> };
              
              return (
                <FooterView footer={ data.eventByName.footer }/>
              )
            }
          }
        </Query>
      </section>
    )
  }  
}