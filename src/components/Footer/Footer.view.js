import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { fontSize, colors } from "./../../styles";

export class FooterView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      footer: props.footer
    }
  }

  render() {
    return (
      <section className="footer-view">
        <hr />
        {/* footer */}
        <div> 
          <img src={ this.state.footer.logo.src } height="20"/>
          <pre>{ this.state.footer.content[0] }</pre>
        </div>
      </section>
    )
  }
}