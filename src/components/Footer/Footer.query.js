import gql from "graphql-tag";

export const Query = gql`
query eventByName($name: String!) {
  eventByName(name: $name) {
    footer {
      logo {
        h
        w
        caption
        src
      }
      background {
        h
        w
        caption
      }
      content
    }
  }
}
`