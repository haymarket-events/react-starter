import React, { Component } from 'react';
import { NavMenu } from './NavMenu/NavMenu';
import { Footer } from './Footer/Footer';

export class Layout extends Component {
  render() {
    return (
      <section className="layout">
        <NavMenu />

        {/* routes */}
        <div>{this.props.children}</div>

        <Footer />
      </section>
    )
  }
}