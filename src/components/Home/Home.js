import React, { Component } from 'react';
// import ExchangeRateView from '../view';
import { HomeView } from './Home.view';
import { Query } from "react-apollo";
import { ActivityIndicator } from "react-native-web";

import { fontSize, colors } from "./../../styles";
import { EventQuery } from './Home.query';

export class Home extends Component {
  constructor() {
    super();

    this.state = {
      eventName : `digitalmediaawards-china-2018`,
      query : EventQuery
    }
  }

  render() {
    return (
      <section className="home">
      <h1>Home</h1>
      <Query query={ this.state.query } variables={{ name: this.state.eventName }}>
        {({ loading, error, data }) => {
            if(error) {
              return (<pre>{JSON.stringify(error, null, 2)}</pre>)
            }
            if (loading || !data) { return <ActivityIndicator color={colors.teal} /> };
            
            return (
              <HomeView event={ data.eventByName }/>
            )
          }
        }
      </Query>
      </section>
    )
  }
}