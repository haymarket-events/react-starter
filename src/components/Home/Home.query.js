import gql from "graphql-tag";

export const EventQuery = gql`
query eventByName($name: String!) {
  eventByName(name: $name) {
    partners {
    title
    # Brands
    brands {
      name
      url
    }
  }
  # People
  people {
    name {
      firstName
      lastName
    }
    organization
    jobTitle
    bio
    avatar
  }
  # Contacts
  contacts {
    name {
      firstName
      lastName
    }
    organization
    jobTitle
    bio
    avatar
  }
  id
  name
  startDate
  endDate
  navLogo {
    caption
    src
  }
  navigations {
    sortorder
    item {
      url
      label
      src
    }
  }
  sns {
    sortorder
    item {
      url
      label
      src
    }
  }
  jumbotron {
    logo {
      src
    }
    background {
      src
    }
    content
  }
  footer {
    logo {
      h
      w
      caption
      src
    }
    background {
      h
      w
      caption
    }
    content
  }
  intro {
    description
  }
  photos {
    src
  }
  }
}
`