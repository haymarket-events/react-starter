import React, { Component } from "react";

import { fontSize, colors } from "./../../styles";

export class HomeView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      event: props.event
    }
  }

  render() {
    return (
      <section className="home-view">
        <h1>{ this.state.event.name }</h1>
        <div className="intro">
          {
            (this.state.event.intro.description).map((d, i) => {
              return <pre key={ i }>{ d }</pre>
            })
          }
        </div>
      </section>
    )
  }
}