import React, { Component } from "react";

import { fontSize, colors } from "./../../styles";

export class SpeakerView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      people: props.people
    }
  }

  render() {
    return (
      <section className="speaker-view">
        { 
          (this.state.people).map(p => {
            return (
              <div key={ p.avatar }>
                <h1>{ p.firstName } { p.lastName }</h1>
                <img width="200" src={ p.avatar }/>
                <div>{ p.organization }</div>
                <div>{ p.jobTitle }</div>
                <div>{ p.bio }</div>
              </div>
            )
          }) 
        }
      </section>
    )
  }
}