import gql from "graphql-tag";

export const Query = gql`
query eventByName($name: String!) {
  eventByName(name: $name) {
  # People
  people {
    name {
      firstName
      lastName
    }
    organization
    jobTitle
    bio
    avatar
  }
  }
}
`