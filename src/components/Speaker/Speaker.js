import React, { Component } from 'react';
// import ExchangeRateView from '../view';
import { SpeakerView } from './../Speaker/Speaker.view';
import { Query } from "react-apollo";
import { ActivityIndicator } from "react-native-web";

import { fontSize, colors } from "./../../styles";
import { Query as SpeakerQuery } from './Speaker.query';

export class Speaker extends Component {
  constructor() {
    super();
    this.state = {
      eventName : `digitalmediaawards-china-2018`,
      query : SpeakerQuery
    }
  }

  render() {
    return (
      <section className="speaker">
      <h1>Speaker</h1>
      <Query query={ this.state.query } variables={{ name: this.state.eventName }}>
        {({ loading, error, data }) => {
            if(error) {
              return (<pre>{JSON.stringify(error, null, 2)}</pre>)
            }
            if (loading || !data) { return <ActivityIndicator color={colors.teal} /> };
            
            return (
              <SpeakerView people={ data.eventByName.people }/>
            )
          }
        }
      </Query>
      </section>
    )
  }
}