import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Query } from "react-apollo";

import { ActivityIndicator } from "react-native-web";

import { NavMenuView } from './NavMenu.view';
import { Query as NavMenuQuery } from './NavMenu.query';
import { fontSize, colors } from "./../../styles";

export class NavMenu extends Component {
  constructor() {
    super();
    this.state = {
      eventName : `digitalmediaawards-china-2018`,
      query : NavMenuQuery 
    }
  }
  render() {
    return (
      <section className="nav-menu">
        {/* <div>
          [<Link to="/">Home</Link>]
          [<Link to="/speakers">Speakers</Link>]
        </div>
        <hr /> */}
        <Query query={ this.state.query } variables={{ name: this.state.eventName }}>
          {({ loading, error, data }) => {
              if(error) {
                return (<pre>{JSON.stringify(error, null, 2)}</pre>)
              }
              if (loading || !data) { return <ActivityIndicator color={colors.teal} /> };
              
              return (
                <NavMenuView navigations={ data.eventByName.navigations }/>
              )
            }
          }
        </Query>
        <hr />
      </section>
    )
  }  
}