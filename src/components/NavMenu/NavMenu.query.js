import gql from "graphql-tag";

export const Query = gql`
query eventByName($name: String!) {
  eventByName(name: $name) {
    navigations {
      sortorder
      item {
        url
        label
        src
      }
    }
  }
}
`