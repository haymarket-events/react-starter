import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { fontSize, colors } from "./../../styles";

export class NavMenuView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nav: props.navigations
    }
  }

  render() {
    return (
      <section className="navmenu-view">
        { 
          (this.state.nav).map((n, index) => {
            if ((n.item.url).startsWith('http') || (n.item.url).includes("//")) {
              return (
                <span key={index}>
                <a href={ (n.item.url) } target="_blank" >{ n.item.label }</a>
                </span>
              )
            } else {
              return (
                <span key={index}>
                <Link to={ (n.item.url).substring(1) }>{ n.item.label }</Link>
                </span>
              )
            }
          }) 
        }
      </section>
    )
  }
}